package forcagame.forca;

import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

public class UI {
	
	Scanner input = new Scanner(System.in);
	
	
	public static void printPalavra(String sorteada, char[] acertos) {
    	
    	for(int i=0; i<sorteada.length(); i++) {
    		if(acertos[i] == 0) {
    			System.out.print(" _ ");
    		} else {
    			System.out.print(" " + sorteada.charAt(i) + " ");
    		}
    	}
    	
	}
	
	public static void printLetrasUtilizadas(ArrayList<Character> letras, int tamanhoDoArray) {
		String letrasUtilizadas = "";
		for(int i=0; i<tamanhoDoArray; i++) {
			letrasUtilizadas += letras.get(i) + " - ";
		}
		System.out.println(letrasUtilizadas);
	}
	
	public static void clearScreen() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}
	
	public static void printPontos(int pontos) {
		System.out.print("A Sua pontuação até agora é: " + pontos + "\n");
	}
}
