package forcagame.forca;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Game {
	Random random = new Random();
	
	private String[] dicionario = {"ELEFANTE", "LEAO", "MACACO", "GIRAFA", "PATO", "CISNE"};
	
	private String sorteada = dicionario[random.nextInt(dicionario.length)];
	
	private char[] acertos = new char[sorteada.length()];
	
	private ArrayList<Character> letrasUtilizadas = new ArrayList();
	
	private int pontos = 0;
	
	boolean gameOver = false;
	
	boolean acertou = false;
	
	public String getSorteada() {
		return sorteada;
	}
	
	public char[] getAcertos() {
		return acertos;
	}
	
	public boolean getGameOver() {
		return gameOver;
	}
	
	public void setGameOver(boolean isOver) {
		gameOver = isOver;
	}
	
	public void setAcertos(int indice) {
		acertos[indice] = 1;
	}
	
	public void addLetrasUtilizadas(char letra) {
		letrasUtilizadas.add(letra);
	}
	
	public ArrayList<Character> getLetrasUtilizadas() {
		return letrasUtilizadas;
	}
	
	public int getPontos() {
		return pontos;
	}
	
	public void addPontos() {
		pontos++;
	}
	
	public void addPontos(int p) {
		pontos += p;
	}
	
	public void removePontos() {
		if(pontos >= 3) {
			pontos += -3;
		} else {
			pontos = 0;
		}
	}
	
	public void removePontos(int p) {
		pontos -= p;
	}
	
	public boolean jaExisteLetra(char letra) {
		boolean jaExiste = false;
		for(int i=0; i<letrasUtilizadas.size(); i++) {
			if(letra == letrasUtilizadas.get(i)) {
				jaExiste = true;
				break;
			}
		}
		return jaExiste;
	}
	
	public void makePlay(String letra) {
		//TODA VEZ QUE O JOGADOR DIGITAR UMA LETRA ELA É ARMAZENADA NA VARIAVEL letasUtilizadas
		if(jaExisteLetra(letra.charAt(0))) {
			removePontos(1);
		} else {
			addLetrasUtilizadas(letra.charAt(0));
			acertou = false;
			
			for(int i=0; i<getSorteada().length(); i++) {
		    	if(letra.charAt(0) == sorteada.charAt(i)) {
		    		setAcertos(i);
		    		addPontos();
		    		acertou = true;
		   		}
		   	}
			
			if(acertou == false) {
				removePontos();
			}
		
			
	    	
	    	
	    	setGameOver(true);
	    	
	    	// COMO O GAMEOVER ESTA COMO TRUE, O FOR IRÁ SETAR PARA FALSO SE AINDA TIVER LETRAS A SEREM PREENCHIDAS
	    	for(int i=0; i<getSorteada().length(); i++) {

	    		if(acertos[i] == 0) {
	    			setGameOver(false);
	    		}
	    		
	    	}
		}
		
		
    	
	}
}
