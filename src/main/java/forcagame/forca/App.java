package forcagame.forca;

import java.util.Scanner;

public class App 
{
    public static void main( String[] args )
    {
    	Game game = new Game();
    	
    	Scanner input = new Scanner(System.in);
    	
    	String letra;
    	
    	//ARMAZENA VARIAVEL SORTEADA UMA PALAVRA SORTEADA DO DICIONARIO
    	String sorteada = game.getSorteada();
    	
    	//System.out.println(sorteada);
    	while(game.getGameOver() == false) {
    		UI.clearScreen();
    		UI.printPalavra(sorteada, game.getAcertos());
    		
    		System.out.println("\n\nLetras Utilizadas:\n");
    		
    		UI.printLetrasUtilizadas(game.getLetrasUtilizadas(), game.getLetrasUtilizadas().size());
    		UI.printPontos(game.getPontos());
    		
    		System.out.print("\nDigite uma letra:");
    		
    		
        	letra = input.nextLine().toUpperCase();
        	if(letra.equals("SAIR")) {
        		System.out.println("Volte sempre :)");
        		break;
        	} else {
        		game.makePlay(letra);
        		
        	}
        	
        	// CASO O GAMEOVER AINDA ESTIVER TRUE, O JOGO ACABA, PARABENIZANDO O JOGADOR POR ACERTAR A PALAVRA
        	if(game.getGameOver() == true) {
        		UI.clearScreen();
    			UI.printPalavra(sorteada, game.getAcertos());
    			System.out.println("\nParabéns você acertou a palavra e fez " + game.getPontos() + " pontos!!!  :D");
    		}
    	}
    }
}
