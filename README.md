## :fire: Jogo da forca :fire:

:exclamation: Para jogar o game você terá que compilar o projeto, recomendo dar o comando:
````
mvn package
````

:exclamation: Depois para rodar o jogo é só digitar o comando a seguir a partir da pasta raiz do projeto:
````
java -jar target/forca-game-1.0.0-SNAPSHOT.jar
````

:exclamation: Palavras do dicionário da forca:

-Elefante
-Leao
-Macaco
-Girafa
-Pato
-Cisne
